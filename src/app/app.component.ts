import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-tabhotel';


  getMenuClickEvent(event) {
    console.log('Menu click event=', event);
    alert('A click event from menu:' + event.srcElement.firstChild.data);
  }

}
