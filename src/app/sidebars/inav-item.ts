export interface INavItem {
  name: string;
  iconName?: string;
  submenus?: INavItem[];
}
