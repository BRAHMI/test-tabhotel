import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { INavItem } from './inav-item';
import { animate, state, style, transition, trigger } from '@angular/animations';


@Component({
    selector: 'app-sidebar-tabHotel',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    animations: [
        trigger('indicatorRotate', [
            state('collapsed', style({ transform: 'rotate(0deg)' })),
            state('expanded', style({ transform: 'rotate(180deg)' })),
            transition('expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
            ),
        ])
    ]
})
export class SidebarsComponent implements OnInit {
    //EventEmitter for parent component
    @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

    navItems: INavItem[] = [
        {
            name: "link 1",
            iconName: "fa-home fa-lg p-2",
            submenus: [{
                name: "submenu 1",
                iconName: "bi bi-type-h1"
            }],
        },
        {
            name: "link 2",
            iconName: 'fa-sliders',
            submenus: [{
                name: "submenu 2",
                iconName: 'fa-person',
                submenus: [{
                    name: "submenu 3",
                    iconName: "fa-person"
                }],
            }],
        },
    ];


    get show(): boolean {
        return true;
    }

    expanded = {};

    constructor() { }

    onItemSelected(item: INavItem, event) {

        if (item.submenus && item.submenus.length) {
            this.expanded[item.name] = !this.expanded[item.name];
        }

        console.log(this.expanded)

        // Sending the event to app.component.ts
        this.onClick.emit(event);
    }

    ngOnInit() { }

}
